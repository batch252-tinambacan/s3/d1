-- [SECTION] Creating new records
INSERT INTO artist (name) VALUES ("Rivermaya");
INSERT INTO artist (name) VALUES ("Psy");

INSERT INTO albums (album_title, data_released, artist_id) VALUES ("Psy-6", "2012-1-1", 2);
INSERT INTO albums (album_title, data_released, artist_id) VALUES ("Trip", "1996-1-1", 1);

INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Gangnam Style", 253, "k-pop", 1);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Kundiman", 243, "OPM", 2);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Kisapmata", 249, "OPM", 2);

-- [SECTION] Retrieving records from a table
-- Retrieve the title and genre of ALL songs
SELECT song_name, genre FROM songs;

-- Retrieve all records from the songs table
SELECT * FROM songs;

-- Return records with a condition
SELECT song_name FROM songs WHERE genre = "OPM";

-- Within the WHERE clause, we can also use the AND and OR operatora
SELECT song_name, length FROM songs WHERE length > 240 AND genre = "OPM";

-- [SECTION] Updating an existing record from a table
-- This query will update the 'Kundiman' song's lenth to 240
UPDATE songs SET length = 240 WHERE song_name = "Kundiman";

-- [SECTION] Deleting an existing record from a table
DELETE FROM songs WHERE genre = "OPM" AND length > 240;